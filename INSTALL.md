Building KimmyCoin
================

See doc/build-*.md for instructions on building the various
elements of the KimmyCoin Core reference implementation of KimmyCoin.
