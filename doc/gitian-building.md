Gitian building
================

This file was moved to [the KimmyCoin Core documentation repository](https://github.com/kimmycoin-core/docs/blob/master/gitian-building.md) at [https://github.com/kimmycoin-core/docs](https://github.com/kimmycoin-core/docs).
