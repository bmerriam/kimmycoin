// Copyright (c) 2017 The Bitcoin Core developers
// Copyright (c) 2017-2019 The Raven Core developers
// Copyright (c) 2020-2021 The KimmyCoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef KIMMYCOIN_RPC_SAFEMODE_H
#define KIMMYCOIN_RPC_SAFEMODE_H

static const bool DEFAULT_DISABLE_SAFEMODE = true;

void ObserveSafeMode();

#endif // KIMMYCOIN_RPC_SAFEMODE_H
