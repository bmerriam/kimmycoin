// Copyright (c) 2011-2014 The Bitcoin Core developers
// Copyright (c) 2017-2019 The Raven Core developers
// Copyright (c) 2020-2021 The KimmyCoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef KIMMYCOIN_QT_KIMMYCOINADDRESSVALIDATOR_H
#define KIMMYCOIN_QT_KIMMYCOINADDRESSVALIDATOR_H

#include <QValidator>

/** Base58 entry widget validator, checks for valid characters and
 * removes some whitespace.
 */
class KimmyCoinAddressEntryValidator : public QValidator
{
    Q_OBJECT

public:
    explicit KimmyCoinAddressEntryValidator(QObject *parent);

    State validate(QString &input, int &pos) const;
};

/** KimmyCoin address widget validator, checks for a valid kimmycoin address.
 */
class KimmyCoinAddressCheckValidator : public QValidator
{
    Q_OBJECT

public:
    explicit KimmyCoinAddressCheckValidator(QObject *parent);

    State validate(QString &input, int &pos) const;
};

#endif // KIMMYCOIN_QT_KIMMYCOINADDRESSVALIDATOR_H
