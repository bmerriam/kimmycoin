#!/bin/sh

TOPDIR=${TOPDIR:-$(git rev-parse --show-toplevel)}
SRCDIR=${SRCDIR:-$TOPDIR/src}
MANDIR=${MANDIR:-$TOPDIR/doc/man}

KIMMYCOIND=${KIMMYCOIND:-$SRCDIR/kimmycoind}
KIMMYCOINCLI=${KIMMYCOINCLI:-$SRCDIR/kimmycoin-cli}
KIMMYCOINTX=${KIMMYCOINTX:-$SRCDIR/kimmycoin-tx}
KIMMYCOINQT=${KIMMYCOINQT:-$SRCDIR/qt/kimmycoin-qt}

[ ! -x $KIMMYCOIND ] && echo "$KIMMYCOIND not found or not executable." && exit 1

# The autodetected version git tag can screw up manpage output a little bit
KIMMYVER=($($KIMMYCOINCLI --version | head -n1 | awk -F'[ -]' '{ print $6, $7 }'))

# Create a footer file with copyright content.
# This gets autodetected fine for kimmycoind if --version-string is not set,
# but has different outcomes for kimmycoin-qt and kimmycoin-cli.
echo "[COPYRIGHT]" > footer.h2m
$KIMMYCOIND --version | sed -n '1!p' >> footer.h2m

for cmd in $KIMMYCOIND $KIMMYCOINCLI $KIMMYCOINTX $KIMMYCOINQT; do
  cmdname="${cmd##*/}"
  help2man -N --version-string=${KIMMYVER[0]} --include=footer.h2m -o ${MANDIR}/${cmdname}.1 ${cmd}
  sed -i "s/\\\-${KIMMYVER[1]}//g" ${MANDIR}/${cmdname}.1
done

rm -f footer.h2m
