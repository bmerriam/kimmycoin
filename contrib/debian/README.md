
Debian
====================
This directory contains files used to package kimmycoind/kimmycoin-qt
for Debian-based Linux systems. If you compile kimmycoind/kimmycoin-qt yourself, there are some useful files here.

## kimmycoin: URI support ##


kimmycoin-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install kimmycoin-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your kimmycoin-qt binary to `/usr/bin`
and the `../../share/pixmaps/kimmycoin128.png` to `/usr/share/pixmaps`

kimmycoin-qt.protocol (KDE)

