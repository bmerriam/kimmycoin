 #!/usr/bin/env bash

 # Execute this file to install the kimmycoin cli tools into your path on OS X

 CURRENT_LOC="$( cd "$(dirname "$0")" ; pwd -P )"
 LOCATION=${CURRENT_LOC%KimmyCoin-Qt.app*}

 # Ensure that the directory to symlink to exists
 sudo mkdir -p /usr/local/bin

 # Create symlinks to the cli tools
 sudo ln -s ${LOCATION}/KimmyCoin-Qt.app/Contents/MacOS/kimmycoind /usr/local/bin/kimmycoind
 sudo ln -s ${LOCATION}/KimmyCoin-Qt.app/Contents/MacOS/kimmycoin-cli /usr/local/bin/kimmycoin-cli
