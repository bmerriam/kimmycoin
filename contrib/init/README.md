Sample configuration files for:
```
SystemD: kimmycoind.service
Upstart: kimmycoind.conf
OpenRC:  kimmycoind.openrc
         kimmycoind.openrcconf
CentOS:  kimmycoind.init
OS X:    org.kimmycoin.kimmycoind.plist
```
have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
