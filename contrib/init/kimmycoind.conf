description "KimmyCoin Core Daemon"

start on runlevel [2345]
stop on starting rc RUNLEVEL=[016]

env KIMMYCOIND_BIN="/usr/bin/kimmycoind"
env KIMMYCOIND_USER="kimmycoin"
env KIMMYCOIND_GROUP="kimmycoin"
env KIMMYCOIND_PIDDIR="/var/run/kimmycoind"
# upstart can't handle variables constructed with other variables
env KIMMYCOIND_PIDFILE="/var/run/kimmycoind/kimmycoind.pid"
env KIMMYCOIND_CONFIGFILE="/etc/kimmycoin/kimmycoin.conf"
env KIMMYCOIND_DATADIR="/var/lib/kimmycoind"

expect fork

respawn
respawn limit 5 120
kill timeout 60

pre-start script
    # this will catch non-existent config files
    # kimmycoind will check and exit with this very warning, but it can do so
    # long after forking, leaving upstart to think everything started fine.
    # since this is a commonly encountered case on install, just check and
    # warn here.
    if ! grep -qs '^rpcpassword=' "$KIMMYCOIND_CONFIGFILE" ; then
        echo "ERROR: You must set a secure rpcpassword to run kimmycoind."
        echo "The setting must appear in $KIMMYCOIND_CONFIGFILE"
        echo
        echo "This password is security critical to securing wallets "
        echo "and must not be the same as the rpcuser setting."
        echo "You can generate a suitable random password using the following "
        echo "command from the shell:"
        echo
        echo "bash -c 'tr -dc a-zA-Z0-9 < /dev/urandom | head -c32 && echo'"
        echo
        echo "It is recommended that you also set alertnotify so you are "
        echo "notified of problems:"
        echo
        echo "ie: alertnotify=echo %%s | mail -s \"KimmyCoin Alert\"" \
            "admin@foo.com"
        echo
        exit 1
    fi

    mkdir -p "$KIMMYCOIND_PIDDIR"
    chmod 0755 "$KIMMYCOIND_PIDDIR"
    chown $KIMMYCOIND_USER:$KIMMYCOIND_GROUP "$KIMMYCOIND_PIDDIR"
    chown $KIMMYCOIND_USER:$KIMMYCOIND_GROUP "$KIMMYCOIND_CONFIGFILE"
    chmod 0660 "$KIMMYCOIND_CONFIGFILE"
end script

exec start-stop-daemon \
    --start \
    --pidfile "$KIMMYCOIND_PIDFILE" \
    --chuid $KIMMYCOIND_USER:$KIMMYCOIND_GROUP \
    --exec "$KIMMYCOIND_BIN" \
    -- \
    -pid="$KIMMYCOIND_PIDFILE" \
    -conf="$KIMMYCOIND_CONFIGFILE" \
    -datadir="$KIMMYCOIND_DATADIR" \
    -disablewallet \
    -daemon

